import java.util.*;

public class StackMachine
{
    public static void main(String args[])
    {
        System.out.println("Let's have the stack!");//print to the screen
        Scanner scanner = new Scanner (System.in); //create a scanner to handle the input
        Stack stack = new Stack(); //initiate the stack

        while (true){
            if (scanner.next().equals("d")){ //d is for display

                while(!stack.isEmpty()){
                    stack.pop(); //keep popping from the stack until it's empty
                }
            }
            else if (scanner.next().equals("e")){ //e is for execute

                if (stack.pop().equals("+")){ //add the top two 
                    String x = stack.pop().toString();
                    String y = stack.pop().toString();
                    int i = Integer.parseInt(x);
                    int j = Integer.parseInt(y);//turns the top two elements into ints

                    int k = i + j;

                    stack.push(k); //pushes their sum onto the stack
                }

                else if(scanner.next().equals("s")){ //switch the top two
                    String i = stack.pop().toString(); //pull the top two items form the stack, store them
                    String j = stack.pop().toString();

                    stack.push(i);//flip them in space on the stack
                    stack.push(j);
                }


            }

            else if (scanner.next().equals("x")){
                break;//if the user inputs 'x' then break out of the stack machine
            }
            else {
                stack.push(scanner.next());//push the element onto the stack
                System.out.println(" ");//give us another empty line
            }

        }
    }
}
