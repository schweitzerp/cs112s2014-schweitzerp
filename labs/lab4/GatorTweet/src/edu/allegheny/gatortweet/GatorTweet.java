package edu.allegheny.gatortweet; 
import java.util.*;
import java.lang.Object;
import java.io.*;
import twitter4j.*;
/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

import java.util.List;
/**
 * @author Yusuke Yamamoto - yusuke at mac.com
 * @since Twitter4J 2.1.7
 */
public class GatorTweet {

    public static void main (String[] args){
        /**
         * Usage: java twitter4j.examples.timeline.GetHomeTimeline
         *
         * @param args String[]
         */
        ArrayList <Tweet> tweets = new ArrayList <Tweet> ();
        ArrayList <String> strings = new ArrayList <String> ();
        if (args[0].equals("update")){
            File Tweetlist = new File("tweets/tweets.txt");//loads the tweets from the first tweetlist
            try{
                /* Scanner tweetlist = new Scanner (new File ("/tweets/tweets.txt")); */
                Scanner scanner = new Scanner (Tweetlist); //inserts that file into the scanner object
                while (scanner.hasNextLine()){//if there's another line in the file, load it into scanner
                    Tweet tweet = new Tweet(); //create a tweet object
                    String new1 = scanner.nextLine(); //load the line of tweets.txt into the String new1

                    if(tweet.isValidMessage(new1)){
                        tweet.setMessage(new1); //if that's a valid tweet
                        tweets.add(tweet); //add the tweet string to the tweet

                    }
                    else{
                        strings.add(new1); //otherwise dump it into the unused strings arraylist
                    }
                }
            }

            catch(FileNotFoundException e){
                System.out.println("File Not Found"); //if there's a problem, print "File Not Found"
            }

            try {
                ConfigurationBuilder cb = new ConfigurationBuilder(); //create a new Configuration 
                cb.setDebugEnabled(true) //enable debugging
                    .setOAuthConsumerKey("rPtRCCRqdDyoxHS3E2UARA") //these two are keys to validate to twitter
                    .setOAuthConsumerSecret("hhDnR4NETStvN4F84km2xuBy3eXJ8l2FnjdL23YPs");
                // gets Twitter instance with default credentials
                TwitterFactory tf = new TwitterFactory(cb.build()); //creates a new twitterfactory object
                Twitter twitter = tf.getInstance();
                /* Twitter twitter = new TwitterFactory().getInstance(); */
                User user = twitter.verifyCredentials();
                for (int i = 0; i <= tweets.size(); i ++){

                    twitter.updateStatus(tweets.get(i).getMessage()); //update the profile with current tweets (if they aren't duplicates
                }
            } 
            catch (TwitterException te) {
                te.printStackTrace();
                System.out.println("Failed to get timeline: " + te.getMessage()); //tell user if there's a problem
                System.exit(-1);
            }
        }

        if(args[0].equals("timeline")){
            try {
                ConfigurationBuilder cb = new ConfigurationBuilder();
                cb.setDebugEnabled(true)
                    .setOAuthConsumerKey("rPtRCCRqdDyoxHS3E2UARA")
                    .setOAuthConsumerSecret("hhDnR4NETStvN4F84km2xuBy3eXJ8l2FnjdL23YPs");
                // gets Twitter instance with default credentials
                TwitterFactory tf = new TwitterFactory(cb.build());
                Twitter twitter = tf.getInstance();
                /* Twitter twitter = new TwitterFactory().getInstance(); */
                User user = twitter.verifyCredentials(); //check the user's credentials
                List<Status> statuses = twitter.getHomeTimeline(); //get statuses
                System.out.println("Showing @" + user.getScreenName() + "'s home timeline.");
                for (Status status : statuses) { //for all statuses
                    System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText());//print the details
                }
            } catch (TwitterException te) {
                te.printStackTrace();
                System.out.println("Failed to get timeline: " + te.getMessage());
                System.exit(-1);
            }
        }

        if(args[0].equals("otherTweet")){
            File Tweetlist = new File("otherTweets/tweets.txt");

            try{
                /* Scanner tweetlist = new Scanner (new File ("/tweets/tweets.txt")); */
                Scanner scanner = new Scanner (Tweetlist);
                while (scanner.hasNextLine()){
                    Tweet tweet = new Tweet();
                    String new1 = scanner.nextLine();

                    if(tweet.isValidMessage(new1)){
                        tweet.setMessage(new1);
                        tweets.add(tweet); //add the tweet string to the tweet

                    }
                    else{
                        strings.add(new1);
                    }
                }
            }

            catch(FileNotFoundException e){
                System.out.println("File Not Found");
            }

            try {
                ConfigurationBuilder cb = new ConfigurationBuilder();
                cb.setDebugEnabled(true)
                    .setOAuthConsumerKey("rPtRCCRqdDyoxHS3E2UARA")
                    .setOAuthConsumerSecret("hhDnR4NETStvN4F84km2xuBy3eXJ8l2FnjdL23YPs");
                // gets Twitter instance with default credentials
                TwitterFactory tf = new TwitterFactory(cb.build());
                Twitter twitter = tf.getInstance();
                /* Twitter twitter = new TwitterFactory().getInstance(); */
                User user = twitter.verifyCredentials();
                for (int i = 0; i <= tweets.size(); i ++){

                    twitter.updateStatus(tweets.get(i).getMessage()); // 
                }
            } 
            catch (TwitterException te) {
                te.printStackTrace();
                System.out.println("Failed to get timeline: " + te.getMessage());
                System.exit(-1);
            }
        }

    }
}

