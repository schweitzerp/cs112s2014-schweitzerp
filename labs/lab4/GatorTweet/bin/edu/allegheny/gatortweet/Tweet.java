package edu.allegheny.gatortweet;

import java.util.Date;

public class Tweet {

    private Date currentDate; //prepares a container called currentDate which can hold a Date object

    private String message; //Creates an empty String called message

    private static final int MAX_LENGTH = 140; //defines the maximum length of a tweet

    public Tweet() {
        currentDate = new Date(); //currentDate is now declared as a new date
    }

    public boolean isValidMessage(String message) {
        if (message.length() <= MAX_LENGTH && message.length() >= 0)
            return true; //if the tweet is the right length, it returns true
        else
            return false; //otherwise, it returns false
    }

    public void setMessage(String message) {
        this.message = message; //assigns the string of the message to the variable message
    }    

    public String toString() {
        return "(" + currentDate.toString() + ", " + message + ")"; //returns the tweet and date as a string
    }

}
