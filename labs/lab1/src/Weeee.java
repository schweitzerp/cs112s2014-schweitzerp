public class Weeee
{

    public static void weeee()//infinite method recursion
    {
	
	System.out.println("Weeee!");//prints "Weeee!"
	weeee();//throws an error rather quickly

    }

    public static void main(String[] args)
    {

	weeee(); //call the weeee function

    }

}
