/*
 * code adapted from that provided by Professor Kapfhammer (of Allegheny College)
 * by Philip Schweitzer on March 4 2014
 */

package edu.allegheny.benchmark;

import com.clarkware.Profiler; 
import java.io.*;

public class UseFibonacci {

    public static void main(String[] args) {

        System.out.println("Begin experiment with different Fibonacci " +
                "implementations ..."); System.out.println();//prints out information for the beginning

        // extract the value that was passed on the command line; this is the
        // nth fibonacci number that we must calculate in the three different
        // fashions
        Integer Num = new Integer(args[0]); int num = Num.intValue();

        // determine which algorithm we are supposed to benchmark
        String chosen = args[1];
        String latter=args[2]; //determines which data type is used for the benchmark
        if( chosen.equals("recursive") || chosen.equals("all") ) {
            if( latter.equals("int") || latter.equals("all")) {
                // 1. RECURSIVE fibonacci (int)
                Profiler.begin("RecursiveFibonacciInt"); int recursiveFib =
                    RecursiveFibonacci.fib(num); //run profiler for recursiveFib with an int
                    Profiler.end("RecursiveFibonacciInt");

                System.out.println("(Recursive/int) The " + num + "th Fibonacci " +
                        "number = " + recursiveFib + ".");

            }

            if( latter.equals("long") || latter.equals("all")){

                // 1. RECURSIVE fibonacci (long)
                Profiler.begin("RecursiveFibonacciLong"); long recursiveFibLong =
                RecursiveFibonacci.fibLong(num);
            Profiler.end("RecursiveFibonacciLong");

            System.out.println("(Recursive/long) The " + num + "th Fibonacci "
                + "number = " + recursiveFibLong + ".");
            }
        }

        if( chosen.equals("iterative") || chosen.equals("all") ) {
            if( latter.equals("int") || latter.equals("all")) {

                // 2. ITERATIVE fibonacci (int)
                Profiler.begin("IterativeFibonacciInt"); int iterativeFib =
                IterativeFibonacci.fib(num);
            Profiler.end("IterativeFibonacciInt");

            System.out.println("(Iterative/int) The " + num + "th Fibonacci " +
                "number = " + iterativeFib + ".");
            }

            if( latter.equals("long") || latter.equals("all")){
                // 2. ITERATIVE fibonacci (long)
                Profiler.begin("IterativeFibonacciLong"); long iterativeFibLong =
                IterativeFibonacci.fibLong(num);
            Profiler.end("IterativeFibonacciLong");

            System.out.println("(Iterative/long) The " + num + "th Fibonacci "
                + "number = " + iterativeFibLong + ".");
            }
        }

        System.out.println(); Profiler.print(new PrintWriter(System.out));

        System.out.println("... End experiment with different Fibonacci " +
        "implementations");


/*
        String latter=args[2];

        if( latter.equals("int")){

            // 1. RECURSIVE fibonacci (int)
            Profiler.begin("RecursiveFibonacciInt"); int recursiveFib =
                RecursiveFibonacci.fib(num);
            Profiler.end("RecursiveFibonacciInt");

            System.out.println("(Recursive/int) The " + num + "th Fibonacci " +
                    "number = " + recursiveFib + ".");

            // 2. ITERATIVE fibonacci (int)
            Profiler.begin("IterativeFibonacciInt"); int iterativeFib =
                IterativeFibonacci.fib(num);
            Profiler.end("IterativeFibonacciInt");

            System.out.println("(Iterative/int) The " + num + "th Fibonacci " +
                    "number = " + iterativeFib + ".");
        }

        if ( latter.equals("long")){

            // 1. RECURSIVE fibonacci (long)
            Profiler.begin("RecursiveFibonacciLong"); long recursiveFibLong =
                RecursiveFibonacci.fibLong(num);
            Profiler.end("RecursiveFibonacciLong");

            System.out.println("(Recursive/long) The " + num + "th Fibonacci "
                    + "number = " + recursiveFibLong + ".");

            // 2. ITERATIVE fibonacci (long)
            Profiler.begin("IterativeFibonacciLong"); long iterativeFibLong =
                IterativeFibonacci.fibLong(num);
            Profiler.end("IterativeFibonacciLong");

            System.out.println("(Iterative/long) The " + num + "th Fibonacci "
                    + "number = " + iterativeFibLong + ".");
        }*/
    }
}
